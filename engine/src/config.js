const basePath = process.cwd();
const { MODE } = require(`${basePath}/constants/blend_mode.js`);
const { NETWORK } = require(`${basePath}/constants/network.js`);

const network = NETWORK.sol;

// General metadata for Ethereum
const namePrefix = "Hellbender";
const description = "This is test hellbender layers";
const baseUri = "ipfs://NewUriToReplace";

const solanaMetadata = {
  symbol: "HBSCOBY",
  seller_fee_basis_points: 0, // Define how much % you want from secondary market sales 1000 = 10%
  external_url: "https://www.scoby.social/",
  creators: [
    {
      address: "5KGqFmGGhaSDXPznz4TAekDNxyJUf9XYizbWaEyHywAC",
      share: 100,
    },
  ],
};

// If you have selected Solana then the collection starts from 0 automatically
const layerConfigurations = [
  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Teeth" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Tongue" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Mask" },
    ],
  },





  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
    ],
  },


    {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Glasses" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Teeth" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Tongue" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Mask" },
      { name: "Glasses" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Glasses" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
      { name: "Glasses" },
    ],
  },
  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Head" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Teeth" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Tongue" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Mask" },
            { name: "Head" },
    ],
  },





  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
            { name: "WomenHead" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
            { name: "WomenHead" },
    ],
  },


    {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Teeth" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Tongue" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Mask" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Glasses" },
            { name: "WomenHead" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Glasses" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
      { name: "Glasses" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
      { name: "Glasses" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
      { name: "Glasses" },
            { name: "WomenHead" },
    ],
  },
  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Teeth" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Tongue" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Mask" },
    ],
  },





  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
    ],
  },


    {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Glasses" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Teeth" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Tongue" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Mask" },
      { name: "Glasses" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Glasses" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
      { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
      { name: "Glasses" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Head" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Teeth" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Tongue" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Mask" },
            { name: "Head" },
    ],
  },





  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
            { name: "WomenHead" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
            { name: "WomenHead" },
    ],
  },


    {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Teeth" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Tongue" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "Body" },
      { name: "Pants" },
      { name: "Shirts" },
      { name: "Jackets" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Beards" },
      { name: "Mask" },
      { name: "Glasses" },
            { name: "Head" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Glasses" },
            { name: "WomenHead" },
    ],
  },


  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Glasses" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Tattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Tongue" },
      { name: "Glasses" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Teeth" },
      { name: "Tongue" },
      { name: "Glasses" },
            { name: "WomenHead" },
    ],
  },

  {
    growEditionSizeTo: 1000,
    layersOrder: [
      { name: "Background" },
            { name: "Bikes" },
      { name: "WomenBody" },
      { name: "Pants" },
      { name: "WomenShirts" },
      { name: "WomenJackets" },
      { name: "WomenTattoos" },
      { name: "Boots" },
      { name: "Buckle" },
      { name: "Mask" },
      { name: "Glasses" },
            { name: "WomenHead" },
    ],
  },
];

const shuffleLayerConfigurations = false;

const debugLogs = false;

const format = {
  width: 512,
  height: 512,
  smoothing: false,
};

const gif = {
  export: false,
  repeat: 0,
  quality: 100,
  delay: 500,
};

const text = {
  only: false,
  color: "#ffffff",
  size: 20,
  xGap: 40,
  yGap: 40,
  align: "left",
  baseline: "top",
  weight: "regular",
  family: "Courier",
  spacer: " => ",
};

const pixelFormat = {
  ratio: 2 / 128,
};

const background = {
  generate: true,
  brightness: "80%",
  static: false,
  default: "#000000",
};

const extraMetadata = {};

const rarityDelimiter = "#";

const uniqueDnaTorrance = 10000;

const preview = {
  thumbPerRow: 5,
  thumbWidth: 50,
  imageRatio: format.height / format.width,
  imageName: "preview.png",
};

const preview_gif = {
  numberOfImages: 5,
  order: "ASC", // ASC, DESC, MIXED
  repeat: 0,
  quality: 100,
  delay: 500,
  imageName: "preview.gif",
};

module.exports = {
  format,
  baseUri,
  description,
  background,
  uniqueDnaTorrance,
  layerConfigurations,
  rarityDelimiter,
  preview,
  shuffleLayerConfigurations,
  debugLogs,
  extraMetadata,
  pixelFormat,
  text,
  namePrefix,
  network,
  solanaMetadata,
  gif,
  preview_gif,
};
